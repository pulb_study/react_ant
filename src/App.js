import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import 'antd/dist/antd.css';

import Register from "./view/register/register";//登录

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
  }

  render() {
    return (
      <div className="App">
        <HashRouter>
          <Switch>
            <Route exact path="/" component={Register} />
          </Switch>
        </HashRouter>
      </div>
    )
  }
}
