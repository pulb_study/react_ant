import React from 'react';

import {Row, Col}  from 'antd';
import { createHashHistory } from 'history'; // 如果是hash路由
const history = createHashHistory();




export default class register extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            InputUser: "",
        };
    }

    componentDidMount() {//已经被渲染到 DOM 中后运行
        this.setState({comment: 'Hello'});//修改数据
    }
    componentWillMount() {//离开时
        this.setState({comment: 'Hello'});//修改数据
    }


    handleClick = () => {
        console.log('this is:', this);
    }
    render() {
        return (
            <div className="App">
                <Row>
                  <Col span={12}>col-12</Col>
                  <Col span={12}>col-12</Col>
                </Row>
                <Row>
                  <Col span={8}>col-8</Col>
                  <Col span={8}>col-8</Col>
                  <Col span={8}>col-8</Col>
                </Row>
                <Row>
                  <Col span={6}>col-6</Col>
                  <Col span={6}>col-6</Col>
                  <Col span={6}>col-6</Col>
                  <Col span={6}>col-6</Col>
                </Row>
            </div>
        );
    }
}
