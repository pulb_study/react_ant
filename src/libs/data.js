import axios from '@/libs/api.request';
import config from '@/config';

export const indexCount = (account) => { // 主页数据
    let data = account
    return axios.request({
        url: '/admin/charge/indexCount',
        data,
        method: 'post'
    });
};

export const userMoneySum = (account) => { // 主页数据
    let data = account
    return axios.request({
        url: '/admin/stockUser/userMoneySum',
        data,
        method: 'post'
    });
};